﻿using BasicWebServer.Demo.Models;
using BasicWebServer.Server.Controllers;
using BasicWebServer.Server.HTTP;
using System.Text;
using System.Web;

namespace BasicWebServer.Demo.Controllers
{
    public class HomeController : Controller
    {
        private const string FileName = "content.txt";
        public HomeController(Request request)
            : base(request)
        {
        }
        public Response Index()
        {
            return Text("Hello from the server!");
        }
        public Response Redirect()
        {
            return Redirect("https://softuni.org/");
        }
        public Response Html()
        {
            return View();
        }
        public Response HtmlFormPost()
        {
            //string formData = string.Empty;

            //foreach (var (key, value) in this.Request.Form)
            //{
            //    formData += $"{key} - {value}";
            //    formData += Environment.NewLine;
            //}
            //return Text(formData);

            var name = this.Request.Form["Name"];
            var age = this.Request.Form["Age"];

            var model = new FormViewModel{
                Name = name,
                Age = int.Parse(age),
            };

            return View(model);
        }
        public Response Content()
        {
            return View();
        }
        private static async Task<string> DownloadWebSiteContent(string url)
        {
            var httpClient = new HttpClient();

            using (httpClient)
            {
                var response = await httpClient.GetAsync(url);
                var html = await response.Content.ReadAsStringAsync();
                return html.Substring(0, 2000);
            }
        }
        private static async Task DownloadSitesAsTextFile(string fileName, string[] urls)
        {
            var downloads = new List<Task<string>>();
            foreach (var url in urls)
            {
                downloads.Add(DownloadWebSiteContent(url));
            }

            var responses = await Task.WhenAll(downloads);

            var responsesString = string.Join(Environment.NewLine + new string('-', 100), responses);

            await File.WriteAllTextAsync(fileName, responsesString);
        }
        public Response DownloadContent()
        {
            DownloadSitesAsTextFile(FileName, new string[] { "https://judge.softuni.org", "https://softuni.org" });

            return MyFile(FileName);
        }

        //Моя интерпретация. Виж как става!
        public Response Cookies()
        {
            var response = new Response(StatusCode.OK);
            var requestHasCookies = Request.Cookies.Any(c => c.Name != Session.SessionCookieName);
            var bodyText = "";

            if (requestHasCookies)
            {
                var cookieText = new StringBuilder();

                cookieText.AppendLine("<h1>Cookie</h1>");

                cookieText.Append("<table border='1'><tr><th>Name</th><th>Value</th></tr>");

                foreach (var cookie in Request.Cookies)
                {
                    cookieText.Append("<tr>");

                    cookieText.Append($"<td>{HttpUtility.HtmlEncode(cookie.Name)}</td>");

                    cookieText.Append($"<td>{HttpUtility.HtmlEncode(cookie.Value)}</td>");

                    cookieText.Append("</tr>");
                }

                cookieText.Append("</table>");

                bodyText = cookieText.ToString();
                response.Body = bodyText;

                return response;
            }

            bodyText = "<h1>Cookie Set</h1>";
            response.Cookies.Add("My-Cookie", "My-Value");
            response.Cookies.Add("My-Second-Cookie", "My-Second-Value");

            response.Body = bodyText;

            return response;
        }

        public Response MySession()
        {
            var currentDateKey = "CurrentDate";
            var sessionExists = Request.Session.ContainsKey(currentDateKey);
            
            if (sessionExists)
            {
                var currentDate = Request.Session[currentDateKey];
                return Text($"Stored date: {currentDate}!");
            }
            
              return Text("Current date stored");
        }
    }
}
