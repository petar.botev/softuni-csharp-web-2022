﻿using BasicWebServer.Server.Controllers;
using BasicWebServer.Server.HTTP;

namespace BasicWebServer.Demo.Controllers
{
    public class UserController : Controller
    {
        private const string UserName = "user";

        private const string Password = "user123";

        public UserController(Request request) 
            : base(request)
        {
        }

        public Response Login() => View();

        public Response LogInUser()
        {
            Request.Session.Clear();

            var bodyText = "";

            var userNameMatches = Request.Form["Username"] == UserName;
            var userPasswordMatches = Request.Form["Password"] == Password;

            if (userNameMatches && userPasswordMatches)
            {
                if (!Request.Session.ContainsKey(Session.SessionUserKey))
                {
                    Request.Session[Session.SessionUserKey] = "MyUserId";
                    var cookies = new CookieCollection();
                    cookies.Add(Session.SessionCookieName, Request.Session.Id);

                    return ResponseHtml("<h3>Logged Successfully!</h3>", cookies);
                }
                return Html("<h3>Logged Successfully!</h3>");
            }
            
            return Redirect("/Login");
        }

        public Response Logout()
        {
            Request.Session.Clear();

            return Html("<h3>Logged out successfully!</h3>");
        }

        public Response GetUserData()
        {
            if (this.Request.Session.ContainsKey(Session.SessionUserKey))
            {
                return Html($"<h3>Currently logged-in user is with username '{UserController.UserName}' </h3>");
            }

            return Redirect("/Login");
        }
    }
}
