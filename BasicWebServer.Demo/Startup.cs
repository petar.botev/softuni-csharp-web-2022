﻿using BasicWebServer.Demo.Controllers;
using BasicWebServer.Server;
using BasicWebServer.Server.Controllers;
using BasicWebServer.Server.HTTP;
using BasicWebServer.Server.Routing;

namespace BasicWebServer.Demo
{
    public class Startup
    {
        public static async Task Main()
        {
             var server = new HttpServer(routes => routes
                  .MapGet<HomeController>("/", c => c.Index())
                  .MapGet<HomeController>("/Redirect", c => c.Redirect())
                  .MapGet<HomeController>("/HTML", c => c.Html())
                  .MapPost<HomeController>("/HTML", c => c.HtmlFormPost())
                  .MapGet<HomeController>("/Content", c => c.Content())
                  .MapPost<HomeController>("/Content", c => c.DownloadContent())
                  .MapGet<HomeController>("/Cookies", c => c.Cookies())
                  .MapGet<HomeController>("/Session", c => c.MySession())
                  .MapGet<UserController>("/Login", c => c.Login())
                  .MapPost<UserController>("/Login", c => c.LogInUser())
                  .MapGet<UserController>("/Logout", c => c.Logout())
                  .MapGet<UserController>("/UserProfile", c => c.GetUserData()));

            await server.Start();
        }
    }

    public static class RoutingTableExtensions
    {
        public static IRoutingTable MapGet<TController>(this IRoutingTable routingTable,
                string path, 
                Func<TController, Response> controllerFunction) where TController : Controller
            => routingTable.MapGet(path, 
                request => controllerFunction(CreateController<TController>(request)));


        public static IRoutingTable MapPost<TController>(this IRoutingTable routingTable,
                string path, 
                Func<TController, Response> controllerFunction) where TController : Controller
            => routingTable.MapPost(path, 
                request => controllerFunction(CreateController<TController>(request)));

        private static TController CreateController<TController>(Request request)
        {
            return (TController)Activator.CreateInstance(typeof(TController), new[] { request });
        }
    }
}
