﻿using BasicWebServer.Server.Common;
using BasicWebServer.Server.HTTP;
using BasicWebServer.Server.Responses;

namespace BasicWebServer.Server.Routing
{
    public class RoutingTable : IRoutingTable
    {
        private readonly Dictionary<Method, Dictionary<string, Func<Request, Response>>> routes;

        public RoutingTable()
        {
            this.routes = new Dictionary<Method, Dictionary<string, Func<Request, Response>>>()
            {
                [Method.Get] = new Dictionary<string, Func<Request, Response>>(),
                [Method.Post] = new Dictionary<string, Func<Request, Response>>(),
                [Method.Put] = new Dictionary<string, Func<Request, Response>>(),
                [Method.Delete] = new Dictionary<string, Func<Request, Response>>(),
            };
        }

        public IRoutingTable Map(Method method, string path, Func<Request, Response> response)
        {
            Guard.AgainstNull(path, nameof(path));
            Guard.AgainstNull(response, nameof(response));

            this.routes[method][path] = response;

            return this;
        }

        public IRoutingTable MapGet(string path, Func<Request, Response> response)
        {
            return Map(Method.Get, path, response);
        }

        public IRoutingTable MapPost(string path, Func<Request, Response> response)
        {
            return Map(Method.Post, path, response);
        }

        public Response MatchRequest(Request request)
        {
            var requestMethod = request.Method;
            var requestUrl = request.Url;

            if (!this.routes.ContainsKey(requestMethod) || !this.routes[requestMethod].ContainsKey(requestUrl))
            {
                return new NotFoundResponse();
            }

            var responseFunction = this.routes[requestMethod][requestUrl];

            //Делегат - приема request и връща response
            return responseFunction(request);
        }
    }
}
