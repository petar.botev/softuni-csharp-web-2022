﻿using BasicWebServer.Server.HTTP;
using BasicWebServer.Server.Responses;
using System.Runtime.CompilerServices;

namespace BasicWebServer.Server.Controllers
{
    public abstract class Controller
    {
        protected Controller(Request request)
        {
            this.Request = request;
        }

        protected Request Request { get; private init; }

        protected Response Text(string text) => new TextResponse(text);
        protected Response Html(string text) => new HtmlResponse(text);

        protected Response ResponseHtml(string text, CookieCollection cookies) => new HtmlResponse(text);

        protected Response BadRequest() => new BadRequestResponse();
        protected Response Unauthorized() => new UnauthorizedResponse();
        protected Response NotFound() => new NotFoundResponse();
        protected Response Redirect(string location) => new RedirectResponse(location);
        protected Response MyFile(string fileName) => new TextFileResponse(fileName);
        protected Response View(object model, [CallerMemberName] string viewName = "") 
            => new ViewResponse(viewName, this.GetControllerName(), model);

        protected Response View([CallerMemberName] string viewName = "")
           => new ViewResponse(viewName, this.GetControllerName());

        private string GetControllerName()
        {
            return this.GetType().Name.Replace(nameof(Controller), string.Empty);
        }
    }
}
